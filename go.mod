module gitlab.com/rygoo/raven

go 1.12

require (
	github.com/fatih/color v1.7.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/gofrs/flock v0.7.1
	github.com/libvirt/libvirt-go v5.1.0+incompatible
	github.com/libvirt/libvirt-go-xml v5.1.0+incompatible
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mergetb/go-ping v0.0.0-20181106132940-7b3a8e3ebce2 // indirect
	github.com/sirupsen/logrus v1.4.0
	github.com/spf13/cobra v0.0.3 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
	gitlab.com/mergetb/xir v0.0.0-20190305161030-8a5d5519d3b8
	golang.org/x/net v0.0.0-20190322120337-addf6b3196f6 // indirect
)

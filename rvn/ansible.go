package rvn

import (
	"fmt"
	"io/ioutil"
)

func UpdateTopoAnsibleHosts(
	toponame string, nodes, switches map[string]DomStatus) error {

	block := fmt.Sprintf("[%s]\n", toponame)

	for name, status := range nodes {
		block += fmt.Sprintf(
			"%s "+
				"ansible_host=%s "+
				"ansible_user=rvn "+
				"ansible_ssh_private_key_file=/var/rvn/ssh/rvn\n",
			name, status.IP)
	}
	for name, status := range switches {
		block += fmt.Sprintf(
			"%s "+
				"ansible_host=%s "+
				"ansible_user=rvn "+
				"ansible_ssh_private_key_file=/var/rvn/ssh/rvn\n",
			name, status.IP)
	}

	return ioutil.WriteFile(".rvn/ansible-hosts", []byte(block), 0644)

}
